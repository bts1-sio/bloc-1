## Mission : Wireshark et découvertes des protocoles 
[Abdoulaye COULIBALY](mailto:enseignant.coulibaly@gmail.com), 
Bloc1 - (BTS - SIO) - 23/04/2024



### Objectif

Analyser le trafic réseau généré par des clients et serveurs - zoom sur les messages HTTP,  HTTPS et FTP à l’aide du logiciel Wireshark.

Un **compte rendu** devra être déposé sur GitLab le 04/05/2024 sous les formats Markdown et PDF -> `Bloc 1 Monsieur COULIBALY/Mission2_Wireshark.md` et `Bloc 1 Monsieur COULIBALY/Mission2_Wireshark.pdf`


Liens utiles : 

- Doc officielle wireshark : <https://www.wireshark.org/docs/wsug_html_chunked/ChapterUsing.html>

Quelques tutoriels Wireshark : 
- <http://razik.univ-tln.fr/misc/I42/Wireshark/index.html>
- <https://astuces-informatique.com/wireshark-capturer-filtrer-inspecter-paquets/>
- <https://www.youtube.com/watch?v=5qecyZHL-GU>


## Etape 1 - Installer Wireshark 

- Télécharger le logiciel Wireshark (<https://www.wireshark.org/>) 

- Résumer en quelques phrases, l'utilité de ce logiciel et ce qu'il permettra de réaliser

- Installer Wireshark sur votre machine hôte (en cochant toutes les options utiles)

- Démarrer l'application et essayer de la prendre en main 
	- Quelles fonctionnalités de wireshark semblent intéressantes ? 


## Etape 2 - Analyser le trafic généré par un serveur HTTP

- Répondre à ces questions avant d'éxécuter la suite : 

	- Qu'est ce qu'un serveur HTTP ?

	- Qu'est ce qu'une requête HTTP ?

- Maintenant place à l'action :

1. Démarrer l'application de nouveau
2. Filtrer le trafic HTTP
3. Démarrer la capture sur la bonne interface réseau
4. Réaliser quelques requêtes HTTP
	- vers un serveur web distant 
5. Arrêter la capture 
6. Enregistrer la capture sous `captureHTTP.pcapng`
7. Enregistrer une capture d'écran sous `captureHTTP.png`

- Répondre aux questions suivantes : 

	- Quelles sont les adresses IP du client et du serveur des trames capturées ?

	- Quelle est la méthode pour récupérer une ressource sur le serveur ?

	- Comment différencier les requêtes des réponses ?

	- Comment se nomme le serveur ?


## Etape 3 - Analyser le trafic généré par un serveur HTTPS

- Répondre à ces questions avant d'éxécuter la suite : 

	- À quoi sert le protocole TLS ?

	- À quoi sert le protocole SSL ?

- Maintenant place à l'action :

1. Fermer la capture HTTP
2. Filtrer le trafic HTTPS (Protocole TLS ou SSL)
3. Démarrer la capture sur la bonne interface réseau
4. Réaliser quelques requêtes HTTPS
	- vers un serveur web distant (le vôtre configuré sur AlwaysData)
5. Arrêter la capture 
6. Enregistrer la capture sous `captureHTTPS.pcapng`
7. Enregistrer une capture d'écran sous `captureHTTPS.png`


- Répondre aux questions suivantes : 

	- Quelles sont les adresses IP du client et du serveur des trames capturées ?

	- Comment différencier les requêtes des réponses ?

	- Quelles différences remarquez-vous avec le protocole HTTP ?


## Etape 4 - Analyser le trafic généré par un serveur FTP

- Réponse à ces questions avant d'éxécuter la suite :

	- À quoi sert le protocole FTP ?

	- Que faut-il pour utiliser le protocole FTP ?

- Maitenant place à l'action :

1. Fermer la capture HTTPS
2. Filter le traffic FTP
3. Démarrer la capture sur la bonne interface réseau
4. Réaliser quelques requêtes FTP (avec envoi de fichier)
	- vers un serveur FTP distant (le vôtre configuré sur Alwaysdata)
5. Arrêter la capture
6. Enregistrer la capture sous `captureFTP.pcapng`
7. Enregistrer une capture d'écran sous `captureFTP.png`


- Répondre aux questions suivantes :

	- Comment différencier les requêtes des réponses ?

	- Quelles sont les adresses IP du client et du serveur des trames capturées ?

	- Combien de paires (requête et réponse) ont été échangées suite à votre action décrite en 4. ?


## Toutes vos captures et captures d'écran devront être rassemblées dans un même et seul dossier.






 



